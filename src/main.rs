extern crate chrono;
mod module;
mod testings;
use std::io::{self};
use chrono::prelude::*;
use module::blocks;
use module::blocks::create::Blocks;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
use module::server::tcp_connects;
// Перечисление событий
enum AddBlocks {
    AddName,
    AddBody,
    AddRefs,
}

// Создаем хэш
fn calculate_hash<T: Hash>(t: &T) -> u64 {
    let mut s = DefaultHasher::new();
    t.hash(&mut s);
    s.finish()
}

// Обновление блока
fn read_strings(el: &mut Blocks, filed: AddBlocks){
    let mut buffer = String::new();
    let handle = io::stdin().read_line(&mut buffer);
    match filed {
        AddBlocks::AddName => el.name = buffer.to_string(),
        AddBlocks::AddBody => el.body = buffer.to_string(),
        AddBlocks::AddRefs => {
            let hashs = calculate_hash(el);
            el.refs = buffer.to_string();
            el.hash = hashs;
        },
        _ => println!("Error"),

    }
}

fn main()-> io::Result<()> {
    let mut block = Blocks{name: String::from("0"), body:String::from("0"), refs: String::from("0"), hash: 0x1234, verify: false, date_create: Local::now()};
    println!("Назовите свое имя");
    read_strings(&mut block, AddBlocks::AddName);
    println!("Что хотите написать");
    read_strings(&mut block, AddBlocks::AddBody);
    println!("Кому");
    read_strings(&mut block, AddBlocks::AddRefs);
    blocks::create::initialization_create(block);
    Ok(())
}

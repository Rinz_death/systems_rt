// Импортируем библиотеки для работы с датой
use chrono::prelude::*;
use std::hash::{Hash};
//Структура блока
#[derive(Hash)]
pub struct Blocks {
    pub hash: u64,
    pub name: String,
    pub body: String,
    pub verify:  bool,
    pub refs: String,
    pub date_create: DateTime<Local>
}

// Типаж все что относится к созданию блоков
pub trait CreateArea{
    fn verifications(&mut self);
    fn create_blocks(&self);
}
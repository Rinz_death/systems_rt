pub mod blocks_structs;
pub use self::blocks_structs::Blocks;
pub use self::blocks_structs::CreateArea;
// Методы верификации и создания блока
impl CreateArea for Blocks {
    fn verifications(&mut self) {
        if self.verify == false {
            self.verify = true;
        }
        println!("Верификация пройдена");
    }
    fn create_blocks(&self){
        println!("{}", self.name);
        println!("Create Blocks");
    }
}

// инитициализация для типажа
pub fn initialization_create<T:CreateArea> (mut create: T) {
    create.create_blocks();
    create.verifications();
}
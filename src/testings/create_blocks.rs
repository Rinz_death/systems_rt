extern crate chrono;
use chrono::prelude::*;
use module::blocks;

#[test]
fn test_main(){
    let block = blocks::create::Blocks{name: String::from("name"), body:String::from("body"), refs: String::from("refs"), hash: String::from("hash"), verify: false, date_create: Local::now()};
    blocks::create::initialization_create(block);
}